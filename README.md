# registrar

A toy example of a registrar using CTFMS. You probably want to use something better in prod.

## Target Configuration

The following target configuration entry is needed for this target:

```json
{ 
  // ...
  "registrar": {
    "description": "The registration service for Speedrun CTF 20XX.",
    "details": {
      "Instanced": {
        "image": "registrar",
        "port": 1111,
        "given_registrar_secret": true, // communicates with central as the registrar
        "unauth": true, // does not expect authentication
        "attached_internal": true, // need to communicate with central
        "hostconfig": {
          "LogConfig": {
            "Type": "json-file" // logging is disabled by default, but we won't log much
          }
        },
        "binds": {
          "/path/to/generated/registrar_ca.key": "/key", // key to sign CSRs with
          "/path/to/generated/registrar_ca.pem": "/ca" // the CA cert
        }
      }
    }
  },
  // ...
}
```