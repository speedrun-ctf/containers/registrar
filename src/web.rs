use actix_web::error::InternalError;
use actix_web::http::StatusCode;
use actix_web::web::{BytesMut, Data, Payload};
use actix_web::{get, post, App, HttpResponse, HttpServer, Responder};
use futures::stream::StreamExt;
use openssl::x509::{X509Req, X509};
use std::net::SocketAddr;
use tokio::sync::mpsc::Sender;
use tokio::sync::oneshot;

#[get("/")]
async fn index() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(include_str!("../registrar-website/dist/index.html"))
}

#[get("/assets/index.css")]
async fn css() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/css")
        .body(include_str!("../registrar-website/dist/assets/index.css"))
}

#[get("/assets/index.js")]
async fn js() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/javascript")
        .body(include_str!("../registrar-website/dist/assets/index.js"))
}

#[post("/sign")]
async fn sign_csr(
    sender: Data<Sender<(X509Req, oneshot::Sender<anyhow::Result<X509>>)>>,
    mut body: Payload,
) -> impl Responder {
    let sender = sender.get_ref().clone();

    let mut bytes = BytesMut::new();
    while let Some(item) = body.next().await {
        bytes.extend_from_slice(item?.as_ref());
    }

    let req = X509Req::from_der(bytes.as_ref())
        .map_err(|e| InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR))?;

    let (tx, rx) = oneshot::channel();
    sender
        .send((req, tx))
        .await
        .map_err(|e| InternalError::new(e.to_string(), StatusCode::INTERNAL_SERVER_ERROR))?;

    let cert = rx
        .await
        .map_err(|e| InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR))?
        .map_err(|e| InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR))?;

    let der = cert
        .to_der()
        .map_err(|e| InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR))?;

    HttpResponse::Ok().message_body(der)
}

pub async fn run_server(
    addr: SocketAddr,
    sender: Sender<(X509Req, oneshot::Sender<anyhow::Result<X509>>)>,
) -> anyhow::Result<()> {
    Ok(HttpServer::new(move || {
        App::new()
            .app_data(Data::new(sender.clone()))
            .service(index)
            .service(css)
            .service(js)
            .service(sign_csr)
    })
    .bind(addr)?
    .run()
    .await?)
}
