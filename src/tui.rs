use openssl::x509::{X509Builder, X509Extension, X509NameBuilder, X509Req, X509};
use rand::{CryptoRng, Rng, SeedableRng};
use rand_chacha::ChaCha20Rng;
use std::net::SocketAddr;
use std::str::FromStr;
use tokio::fs::File;
use tokio::io::{AsyncBufReadExt, AsyncReadExt, AsyncWriteExt, BufReader, BufWriter};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::oneshot;
use trust_dns_resolver::AsyncResolver;
use zeroize::Zeroizing;

use common::{
    await_message, send_message, Request, RequestResult, Secret, UserRegistration, Username,
};
use log::info;

pub async fn run_server(
    addr: SocketAddr,
    key_request_channel: Sender<(X509Req, oneshot::Sender<anyhow::Result<X509>>)>,
) -> anyhow::Result<()> {
    let server = TcpListener::bind(addr).await?;
    info!(target: "startup", "Started listening on {}", addr);

    while let Ok((conn, addr)) = server.accept().await {
        let key_request_channel = key_request_channel.clone();
        info!(target: "server", "Accepted connection from {}", addr);
        let _ = tokio::spawn(async move { handle_connection(conn, key_request_channel).await });
    }

    Ok(())
}

async fn handle_connection(
    mut conn: TcpStream,
    sender: Sender<(X509Req, oneshot::Sender<anyhow::Result<X509>>)>,
) -> anyhow::Result<()> {
    let (rx, tx) = conn.split();
    let mut reader = BufReader::new(rx);
    let mut writer = BufWriter::new(tx);

    writer
        .write_all(
            br#"Welcome to Speedrun CTF!

Existing users: make sure that you connect using your client certificate.

To make a new account, please enter a username: "#,
        )
        .await?;
    writer.flush().await?;

    let mut name = String::new();
    let _ = reader.read_line(&mut name).await?;
    let name = match Username::from_str(name.trim()) {
        Err(e) => {
            writer
                .write_all(
                    format!(
                        "Encountered an error while processing the username: {:?}\n",
                        e
                    )
                    .as_bytes(),
                )
                .await?;
            writer.flush().await?;
            return Err(e.into());
        }
        Ok(n) => n,
    };

    writer.write_u8(b'\n').await?;
    writer
        .write_all(
            format!(
                "Welcome, {}! Please execute the following commands in a separate command.\n",
                name.get_name()
            )
            .as_bytes(),
        )
        .await?;
    writer
        .write_all(
            format!(
                r#"> openssl genrsa -out speedrun-ctf.key.pem 4096
This command generates a new private key that you can use to decrypt messages from the server.

> openssl req -new -key speedrun-ctf.key.pem -out speedrun-ctf.csr
This command generates a certificate signing request so that we can verify your connection.
Fill each of the fields out as you see fit, but make sure to set the Common Name to "{}"!
"#,
                name.get_name()
            )
            .as_bytes(),
        )
        .await?;
    writer.write_u8(b'\n').await?;
    writer
        .write_all(b"Once you've completed these commands, press enter.")
        .await?;
    writer.flush().await?;

    while reader.read_u8().await? != b'\n' {}

    writer
        .write_all(
            b"Great! Now please send content of speedrun-ctf.csr so we can get to signing!\n",
        )
        .await?;
    writer.flush().await?;

    let csr = {
        let mut content = Vec::new();
        loop {
            if content.ends_with(b"-----END CERTIFICATE REQUEST-----\n") {
                break;
            }
            let _ = reader.read_until(b'\n', &mut content).await?;
        }
        match X509Req::from_pem(&content) {
            Ok(csr) => csr,
            Err(e) => {
                writer
                    .write_all(
                        format!("Encountered an error while processing your CSR: {}\n", e)
                            .as_bytes(),
                    )
                    .await?;
                writer.flush().await?;
                return Err(e.into());
            }
        }
    };

    writer.write_all(b"Signing your request now...\n\n").await?;
    writer.flush().await?;

    let (tx, rx) = oneshot::channel();
    if sender.send((csr, tx)).await.is_err() {
        panic!("Something terrible has happened: our sender is dead!");
    }

    match rx.await? {
        Ok(cert) => {
            writer
                .write_all(b"Save the following certificate as speedrun-ctf.crt:\n")
                .await?;
            writer.write_all(&cert.to_pem()?).await?;
        }
        Err(e) => {
            writer
                .write_all(
                    format!("Encountered an error while signing your CSR: {:?}\n", e).as_bytes(),
                )
                .await?;
            writer.flush().await?;
            return Err(e);
        }
    }

    writer.write_all(b"Congratulations! You are now registered. Please connect using the client certificate we've just provided.\n").await?;
    writer.flush().await?;

    Ok(())
}
